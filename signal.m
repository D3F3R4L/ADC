function [out,outs,t,ts ] = signal(freq,M,Amp,dur) % Função para a criação do sinal de entrada
freqA=freq*50; % Frequencia de captura do sinal
t=0:1/freqA:dur; % tempo de duranção do sinal analogioco
freqN= freq*M; % frequencia de amostragem
out = Amp*sin((2*pi*t)/freq); % Sinal analogioco
out=abs(out);
ts=0:1/freqN:dur; % tempo de duração do sinal de amostragem
outs = Amp*sin((2*pi*ts)/freq);% Sinal de amostragem
outs=abs(outs);
end