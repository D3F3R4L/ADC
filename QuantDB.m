function [ out ] = QuantDB(sig,bits)% Fun��o para a Quantiza��o Bit Depth
out=zeros(1,length(sig)); % Vetor de Saida
x=min(sig); %Valor minimo possivel da amplitude do sinal
Vref = max(sig)-x; % Valor de referencia
Res=Vref/(2^bits-1); % resolu��o.
%la�o principal
for i=1:length(sig)
    v=sig(i);
    L=round((v-x)/Res);% L = Posi��o mais proxima do valor de entrada analogico para a nossa quantiza��o. 
    xq=x+L*Res; % Ser� o valor quantizado.
    out(i)=xq; % Guarda no vetor
end
end