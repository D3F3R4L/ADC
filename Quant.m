function [ out ] = Quant(sig,bits)% Função para a Quantização Sucessive Aproximation Convertion
out=zeros(1,length(sig));
Vref = max(sig)-min(sig); % Valor de referencia
Res=Vref/(2^bits-1);% resolução.
bin=zeros(1,bits);
%laço principal
for i=1:length(sig)
    Sac=Vref/2;% Valor auxiliar
    Sac2=0;% Valor auxiliar
    v=sig(i);
    for j=1:bits
        Sum=Sac/2^j;% Valor auxiliar
        if Sac<=v %comparar para saber a resposta em byte
            bin(j)=1;
            Sac2=Sac;
            Sac=Sac+Sum;
        else
            bin(j)=0;
            Sac=Sac2+Sum;
        end
    end
    num=num2str(bin);%trazer o vetor de binarios para uma string
    num=bin2dec(num);%trazer a string o valor em decimal
    out(i)=num*Res; %vetor de saida
end
end